FROM python:3.11-slim

WORKDIR /app

COPY pyproject.toml poetry.lock /app/

RUN pip install poetry==1.8.2 && \
    poetry config virtualenvs.create false && \
    poetry install --no-root --no-dev

ENTRYPOINT ["poetry", "run", "jupyter", "notebook", "--ip=0.0.0.0", "--port=8888", "--allow-root"]
